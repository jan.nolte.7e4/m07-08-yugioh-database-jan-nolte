package cat.itb.yugiohcardsviewer

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cat.itb.yugiohcardsviewer.DataClasses.CardYugioh
import cat.itb.yugiohcardsviewer.DataClasses.Data
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.ArrayList

class AppViewModel: ViewModel() {
    var cards = MutableLiveData<Data>()
    val repository= Repository()
    init {
        fetchData("cardinfo.php")
    }
    fun fetchData(url: String){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.getAllCards(url) }
            if(response.isSuccessful){
                cards.postValue(response.body())
            }
            else{
                Log.e("Error :", response.message())
            }
        }
    }

}