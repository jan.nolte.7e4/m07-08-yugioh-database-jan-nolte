package cat.itb.yugiohcardsviewer.DataBase

import android.app.Application
import androidx.room.Room

class CardApplication:Application() {
    companion object{
        lateinit var database: CardDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this, CardDatabase::class.java, "CardDatabase").build()
    }
}