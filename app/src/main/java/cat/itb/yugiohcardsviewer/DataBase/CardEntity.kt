package cat.itb.yugiohcardsviewer.DataBase

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "CardEntity")
data class CardEntity (
    @PrimaryKey(autoGenerate = false) var id: Int,
    var name:String,
    var cardImage : String
)