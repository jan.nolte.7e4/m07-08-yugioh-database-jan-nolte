package cat.itb.yugiohcardsviewer.DataBase

import androidx.room.*

@Dao
interface CardsDAO {
    @Query("SELECT * FROM CardEntity")
    fun getCollection():MutableList<CardEntity>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addToCollection(cardEntity: CardEntity)
    @Update
    fun updateInCollection(cardEntity: CardEntity)
    @Delete
    fun deleteFromCollection(cardEntity: CardEntity)
    @Query("SELECT COUNT(*) FROM CardEntity WHERE id LIKE :search")
    fun searchInCollection(search:Int): Int
}