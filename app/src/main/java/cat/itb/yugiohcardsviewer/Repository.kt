package cat.itb.yugiohcardsviewer

import androidx.lifecycle.MutableLiveData
import cat.itb.yugiohcardsviewer.DataClasses.Data



class Repository {
    private val apiInterface = ApiInterface.create()
    suspend fun getAllCards(url:String) = apiInterface.getCards(url)
}