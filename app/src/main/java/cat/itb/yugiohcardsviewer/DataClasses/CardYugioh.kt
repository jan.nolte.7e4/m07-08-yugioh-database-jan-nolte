package cat.itb.yugiohcardsviewer.DataClasses

data class CardYugioh(
    val atk: Int,
    val attribute: String,
    val card_images: List<CardImage>,
    val card_prices: List<CardPrice>,
    val card_sets: List<CardSet>,
    val def: Int,
    val desc: String,
    val id: Int,
    val level: Int,
    val name: String,
    val race: String,
    val type: String
)