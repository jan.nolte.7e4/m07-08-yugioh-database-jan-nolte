package cat.itb.yugiohcardsviewer.DataClasses

data class CardImage(
    val id: Int,
    val image_url: String,
    val image_url_small: String
)