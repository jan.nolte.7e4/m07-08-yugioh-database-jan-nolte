package cat.itb.yugiohcardsviewer.DataClasses

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("data")
    val `cardList`: List<CardYugioh>
)