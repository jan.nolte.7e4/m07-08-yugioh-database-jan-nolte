package cat.itb.yugiohcardsviewer

import android.content.Context
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.ActivityNavigator
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import cat.itb.yugiohcardsviewer.DataClasses.CardYugioh
import cat.itb.yugiohcardsviewer.databinding.CardListBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class CardsListAdapter(private val cards:List<CardYugioh>): RecyclerView.Adapter<CardsListAdapter.CardListViewHolder>() {
    lateinit var context: Context
    lateinit var binding: CardListBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_list, parent, false)
        context = parent.context
        binding = CardListBinding.bind(view)
        return CardListViewHolder(view)
    }

    override fun onBindViewHolder(holder: CardListViewHolder, position: Int) {
        holder.bindData(cards[position])
        holder.itemView.setOnClickListener{
            val directions = FragmentListCardsDirections.actionFragmentListCardsToFragmentCardDetail(position)
            Navigation.findNavController(it).navigate(directions)
        }
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    inner class CardListViewHolder (itemView: View) :RecyclerView.ViewHolder(itemView) {
        init {
        }
        fun bindData(card: CardYugioh){
            // card.card_images.get(0).image_url
            //cardImage.setImageResource()
            Glide.with(context)
                .load(card.card_images[0].image_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(binding.cardListImage)
        }
    }
}
